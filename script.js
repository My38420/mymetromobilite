/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
// eslint-disable-next-line no-undef
jQuery('document').ready(function ($) {

	// initialisation de la carte à l'affichage d'une ligne centrée sur Grenoble
	const mymap = L.map('mapid').setView([45.1779, 5.7321], 13);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibXlyaWFtc2ltcGxvbiIsImEiOiJja2Vqdmh1YmgxYnQ3MzFxYmFnc3UyMnd5In0.zhrTSulbycCCVOo0DctSww', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		accessToken: 'pk.eyJ1IjoibXlyaWFtc2ltcGxvbiIsImEiOiJja2Vqdmh1YmgxYnQ3MzFxYmFnc3UyMnd5In0.zhrTSulbycCCVOo0DctSww'
	})
		.addTo(mymap);

	// au chargement de la page d'accueil, on cache la partie concernant les détails d'une ligne
	$(".classe-horaires").hide()
	// $('#actu').fadeIn("slow")

	$('#lignesncf').hide()
	$(".close").click(function () {
		$('#actu').animate({ opacity: 0 });
		$('#actu').hide();
		setTimeout(afficheActu, 20000)

	});


	// quand on est sur une page détail de ligne et qu'on veut revenir sur la liste des lignes
	$(".retour").click(function () {

		$(".classe-horaires").hide()
		if (localStorage.length > 0) {
			// recupFavoris()
			$('#tabfavoris').show()
		}

		$(".section-codes").show()

	});
	$(".retourSncf").click(function () {
		$("#lignesncf").hide()
		if (localStorage.length > 0) {
			// recupFavoris()
			$('#tabfavoris').show()
		}

		$(".section-codes").show()
	});
	bntRetourSncf
	function afficheActu() {
		// $('#actu').fadeIn("slow")*
		$('#actu').animate({ opacity: 1 });

	}
	setTimeout(afficheActu, 2000)

	let url = 'http://data.metromobilite.fr/api/routers/default/index/routes'

	const tag = 'SEM'
	const tougo = "GSV"
	const tpv = 'TPV'
	const SNC = 'SNC'
	let timedeb = Date.now()
	let sens = 0
	let depart = " "
	let dirdef = 0
	let dirret = 0
	let myprevtime = 0
	let mysuivtime = 0
	let direction = ' '
	let idt = ' '
	let mode = ' '
	let shortname = ' '
	let ligneJson = ' ';
	let couleur = ' '
	let horaires = $('#tabHor');

	// appel de l'API pour aller récupérer toutes les lignes accessibles autour de Grenoble
	$.ajax({
		url: url,
		type: "GET", // par défaut
		dataType: "json", // ou HTML par ex
	}).done(function (data) {
		console.warn('DONE');
		console.log(data);
		// appel de la fonction qui liste toutes les lignes
		listing(data);

		$(".button").click(function () {
			// On a cliqué sur un numéro de ligne, on cache la partie liste des lignes

			// $('#actu').hide()
			$('#tabHor').empty()
			$('.section-codes').hide();
			$('.tabfavoris').hide()
			$('h4').hide();

			console.log('init des zones ligne au clic sur un num ')
			direction = this.title
			console.log('direction')
			console.log(direction)
			idt = this.id
			// libellemode.innerText = this.name
			sessionStorage.setItem('idTransp', idt)
			let numl = idt.substring(4, idt.length);
			mode = this.name
			shortname = numl

			$('#libellemode').text(mode)
			$('#libellenuml').text(numl)
			affiche(mode, idt, direction, timedeb, sens)
			if (idt.includes(SNC) === false) 
			{
				$(".classe-horaires").show()
			}
			

		});

	}).fail(function (error) {
		console.warn('FAILLLLL');
		console.log(error);
	}).always(function () {
		console.warn('ALWAYS');
		console.log('Terminé !');
	});
	// $("#Tag").hide
	$("#Tag").click(function () {
		console.log('click tag')
		$('#Tram').toggle()
		$('#Chrono').toggle()
		$('#Flexo').toggle()
		$('#Proximo').toggle()
		// $('.listeTag').css("display : block")
	});
	function recupFavoris() {
		console.log("recup favoris")
		//  Je regarde et récupère mes éléments dans le localStorage
		for (let i = 0; i < localStorage.length; i++) {

			// Les datas du local storage : clef + valeur
			let key = localStorage.key(i)
			let value = localStorage.getItem(key)
			// Je créé un objet pour ma ligne

			const objectUser = JSON.parse(value);
			console.log(objectUser)
			const model = objectUser.mode;
			const idl = objectUser.idt
			const name = objectUser.shortname
			direction = objectUser.direction
			let btn = '<button class="button" id="' + idl + '" name="' + model + '" title = "' + direction + '">' + model + ' : ' + name + '</button>'
			console.log('recup favori objectUser')

			console.log(btn)
			$(btn).appendTo('#tabfavoris');

		}
		$('#tabfavoris').show()
	}
	function suppFavoris() {
		console.log("supp favoris")

		let item = $(this).parent(".tabfavoris");
		let itemStorage = item.attr("id");
		console.log(itemStorage)
		// J'enlève mon item du local storage
		item.remove();

		// Local Storage
		localStorage.removeItem(itemStorage);
		console.log('supfav')
		if (localStorage.length === 0) {
			$('.tabfavoris').hide()
		}

	}
	$("#Tram").click(function () {
		$('#listeTram').toggle()
	});
	$("#Chrono").click(function () {
		$('#listeChrono').toggle()
	});
	$("#Flexo").click(function () {
		$('#listeFlexo').toggle()
	});
	$("#Proximo").click(function () {
		$('#listeProximo').toggle()
	});

	$("#Tougo").click(function () {
		$('#listeTougo').toggle()
	});

	$("#Transisere").click(function () {
		$('#listeTransisere').toggle()
	})
	$("#Voironnais").click(function () {
		$('#listeVoironnais').toggle()
	})
	$("#Sncf").click(function () {
		$('#listeSncf').toggle()
	})
	$("#addfavori").click(function () {
		console.log("clic ajout favori")

		// si la ligne n'existe pas en local storage, on la rajoute 
		if (localStorage.getItem('ligne' + idt) !== null) {
			console.log("favori existe déjà")
		}
		else {
			const ligne = {
				idt,
				mode,
				direction,
				shortname
			}

			console.log(ligne)

			// Stockage des valeurs
			localStorage.setItem('ligne' + idt, JSON.stringify(ligne));
		}
	})
	$("#supfavori").click(function () {
		console.log("clic sup favori")  //Récup des valeurs

		// si la ligne existe en local storage, on la supprime 
		if (localStorage.getItem('ligne' + idt) !== null) {

			const ligne = {
				idt,
				mode,
				direction,
				shortname
			}

			console.log(ligne)

			// suppression de la ligne dans favori
			localStorage.removeItem('ligne' + idt, JSON.stringify(ligne));
		}
		if (localStorage.length !== 0) {
			suppFavoris()
		}
	})
	// fonction de conversion du timestamp en heures locales
	function conversion_heure(time) {
		let reste = time;
		let result = '';

		let nbHours = Math.floor(reste / 3600);
		reste -= nbHours * 3600;

		let nbMinutes = Math.floor(reste / 60);
		reste -= nbMinutes * 60;

		if (nbHours > 0) {
			if (nbHours < 10) {
				result = '0' + result + nbHours + ': ';
			} else {
				result = result + nbHours + ': ';
			}
		}
		if (nbHours === 0) {
			result = '00:'
		}
		if (nbMinutes > 0) {
			if (nbMinutes < 10) {
				result = result + '0' + nbMinutes;
			} else {
				result = result + nbMinutes;
			}
		}
		if (nbMinutes === 0) {
			result = result + '00'
		}
		return result;
	}
	// on demande par la fléche gauche les horaires précédents
	$("#bntPrec").click(function () {
		timedeb = sessionStorage.getItem("timeprec")
		$('#tabHor').empty()
		affiche(mode, idt, direction, timedeb, sens)

		$(".classe-horaires").show()

	});

	// on demande par la fléche droite les horaires suivants
	$("#bntSuiv").click(function () {
		timedeb = sessionStorage.getItem("timesuiv")
		$('#tabHor').empty()
		affiche(mode, idt, direction, timedeb, sens)

		$(".classe-horaires").show()

	});

	// on demande de changer de direction, on affiche les horaires inversés
	$("#bntSens").click(function () {

		if (sens === 0) {

			sens = 1
			console.log('direction dans sens click = 0 :')
		}
		else {
			sens = 0
			console.log('direction dans sens click  = 1:')

		}
		$('#tabHor').empty()
		affiche(mode, idt, direction, timedeb, sens)

		$(".classe-horaires").show()
	});
	function listhoraires(transp) {
		dirdef = transp[0];
		dirret = transp[1];
		console.log('listhoraires')
		console.log(sens)
		console.log(dirret)
		console.log(direction)
		horaires = $('#tabHor');
		myprevtime = dirdef.prevTime;
		mysuivtime = dirdef.nextTime;
		sessionStorage.setItem('timeprec', myprevtime)
		sessionStorage.setItem('timesuiv', mysuivtime)

		// on cherche quel type de libellé de ligne est associé à la ligne
		// pour pouvoir afficher la bonne direction au changement de sens
		let express = false
		let Voironnais = false
		if (idt.includes(tpv)) {
			Voironnais = true
		}
		// les lignes TAG ont un libellé avec / comme séparateur départ/ destination
		let searchTerm = '/';
		let IndexOf = direction.indexOf(searchTerm);
		let lastIndex = direction.lastIndexOf(searchTerm);
		if (IndexOf === -1) {
			searchTerm = 'EXPRESS'
			IndexOf = direction.indexOf(searchTerm);

			if (IndexOf === -1) {
				//recherche du séparateur '-' cas autre que EXPRESS
				searchTerm = ' - ';
				lastIndex = direction.indexOf(searchTerm);
				IndexOf = direction.lastIndexOf(searchTerm);
			}
			else { //recherche du séparateur '(EXP-', cas ligne EXPRESS

				let searchEXP = '(EXP-'
				let indexEXP = direction.lastIndexOf(searchEXP)

				if (indexEXP !== -1) {
					express = true
					let directshort = direction.substring(0, indexEXP)
					console.log(directshort)
					direction = directshort
					searchTerm = '-';
					IndexOf = direction.indexOf(searchTerm);
					lastIndex = direction.lastIndexOf(searchTerm);
				} else {
					//recherche du séparateur '-', cas autre ligne 
					searchTerm = '-';
					IndexOf = direction.indexOf(searchTerm);
					lastIndex = direction.lastIndexOf(searchTerm);

				}

			}

		}

		if (sens === 1) { //sens inverse que poposé par défaut

			if (express === true) {
				// cas des lignes express dont le libellé commence par EXPRESS
				// on ne veut pas le mettre dans la direction
				depart = direction.substring(IndexOf + 8, IndexOf);
			}
			else {
				if (Voironnais === true) {
					// cas des lignes du voironnais dont le libellé  ne contient
					// aucun séparateur
					depart = dirdef.arrets[0].stopName;
				}
				else {
					depart = direction.substring(0, IndexOf);
				}
			}

			$('#texteDirect').text(depart);
			for (let i = 0; i < dirret.arrets.length; i++) {
				console.log(dirret.arrets[i].stopName)
				let time = dirret.arrets[i].trips[0]
				let restime = conversion_heure(time)
				let time2 = dirret.arrets[i].trips[2]
				let restime2 = conversion_heure(time2)
				let time1 = dirret.arrets[i].trips[1]
				let restime1 = conversion_heure(time1)
				let time3 = dirret.arrets[i].trips[3]
				let restime3 = conversion_heure(time3)
				console.log(time)
				console.log(dirret.arrets[i].trips[1])
				let ligneTr = `<tr></tr>`;
				let ligneA = `<td class="ligneHor">` + dirret.arrets[i].stopName + ` </td>`;
				let ligneH1 = `<td class="ligneHor"> ` + restime + ` </td>`;
				let ligneH2 = `<td class="ligneHor"> ` + restime1 + ` </td>`;
				let ligneH3 = `<td class="ligneHor"> ` + restime2 + ` </td>`;
				let ligneH4 = `<td class="ligneHor"> ` + restime3 + ` </td>`;
				console.log(ligneH1)
				$(horaires).append(ligneTr);
				$(horaires).append(ligneA);
				$(horaires).append(ligneH1);
				$(horaires).append(ligneH2);
				$(horaires).append(ligneH3);
				$(horaires).append(ligneH4);
			}
		} else {//sens par défaut

			let depart = direction.substring(lastIndex + 1, direction.length);
			if (Voironnais === true) {
				// cas des lignes du voironnais dont le libellé  ne contient
				// aucun séparateur
				depart = dirret.arrets[0].stopName;
			}
			if (express === true) {
				// 	// cas des lignes express dont le libellé commence par EXPRESS
				// 	// on ne veut pas le mettre dans la direction
				depart = direction.substring(lastIndex, direction.length - 6);
				console.log(depart)
			}
			// const dest = direction.substring(lastIndexOf + 1, direction.length);

			$('#texteDirect').text(depart)
			for (let i = 0; i < dirdef.arrets.length; i++) {
				console.log(dirdef.arrets[i].stopName)
				let time = dirdef.arrets[i].trips[0]
				let restime = conversion_heure(time)
				let time2 = dirdef.arrets[i].trips[2]
				let restime2 = conversion_heure(time2)
				let time1 = dirdef.arrets[i].trips[1]
				let restime1 = conversion_heure(time1)
				let time3 = dirdef.arrets[i].trips[3]
				let restime3 = conversion_heure(time3)
				let ligneTr = `<tr></tr>`;
				let ligneA = ` <td class="ligneHor">` + dirdef.arrets[i].stopName + `</td>`;
				let ligneH1 = `<td class="ligneHor">` + restime + `</td>`;
				let ligneH2 = `<td class="ligneHor">` + restime1 + `</td>`;
				let ligneH3 = `<td class="ligneHor">` + restime2 + `</td>`;
				let ligneH4 = `<td class="ligneHor">` + restime3 + `</td>`;
				console.log(ligneH1)
				$(horaires).append(ligneTr);
				$(horaires).append(ligneA);
				$(horaires).append(ligneH1);
				$(horaires).append(ligneH2);
				$(horaires).append(ligneH3);
				$(horaires).append(ligneH4);


			}
		}

	}
	function affiche(mode, idt, direction, timedeb, sens) {
		console.log('entree affiche : mode idt direction timedeb sens)')
		console.log(mode)
		console.log(idt)
		console.log(direction)
		console.log(timedeb)
		console.log(sens)

		if (idt.includes(SNC))  {
			$('#lignesncf').show()
		}
		else {


			// L.marker([51.505, -0.09]).addTo(mymap).bindPopup('TOTO').openPopup();

			// L.marker([51.505, -0.12]).addTo(mymap).bindPopup('TATA').openPopup();

			const urlPlan = `http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${idt}`
			console.log("acces coordonnées")
			console.log(urlPlan)
			$.ajax({
				url: urlPlan,
				type: "GET", // par défaut
				dataType: "json", // ou HTML par ex
			}).done(function (data) {
				console.warn('DONE');
				// if (ligneJson !== ' ') {
				// 	console.log('effacement tracé précédent')
				// 	L.geoJSON.clearTo(mymap)
				// }
				ligneJson = data.features[0].geometry;
				couleur = data.features[0].properties.COULEUR
				console.log('recup json coord ok')
				console.log(ligneJson);
				console.log(couleur)
				let myStyle = {
					"color": `rgb(${couleur})`,
					"weight": 5,
					"opacity": 0.65
				};
				console.log(myStyle)
				L.geoJSON(ligneJson, {
					style: myStyle
				}).addTo(mymap);
				console.log(ligneJson.coordinates)
				// ligneJson.coordinates.forEach(element => {
				// 	L.marker([51.505, -0.09]).addTo(mymap);
				// });


				// L.marker([51.505, -0.12]).addTo(mymap).bindPopup('TATA').openPopup();



			}).fail(function (error) {
				console.warn('FAILLLLL');
				console.log(error);
			}).always(function () {
				console.warn('ALWAYS');
				console.log('appel url carte Terminé !');
			});

			// $('.container').empty();

			// récupérer tous les arrêts et 4 horaires courants
			url = `http://data.metromobilite.fr/api/ficheHoraires/json?route=${idt}&time=${timedeb}`
			console.log(url)
			$.ajax({
				url: url,
				type: "GET", // par défaut
				dataType: "json", // ou HTML par ex
			}).done(function (transp) {
				console.warn('DONE');
				console.log(transp);
				$(idt).appendTo('#num')

				listhoraires(transp);

			}).fail(function (error) {
				console.warn('FAILLLLL');
				console.log(error);
			}).always(function () {
				console.warn('ALWAYS');
				console.log('Terminé !');
			});
		}

	}

	// pour chaque ligne de transport récupérée, création d'un bouton 
	// dans la liste appropriée
	function listing(data) {
		let transp = data;
		if (localStorage.length !== 0) {
			recupFavoris()
		}
		for (let i = 0; i < transp.length; i++) {

			const id = transp[i].id
			let btn = '<button class="button tabhor" id="' + id + '" name="' + transp[i].mode + '" title = "' + transp[i].longName + '"  style="color : #' + transp[i].color + '">' + transp[i].shortName + '</button>';

			if (id.includes(tag)) {
				$("#Tram").hide();
				$("#Chrono").hide();
				$("#Flexo").hide();
				$("#Proximo").hide();
				// on ajoute le transport à la liste TAG

				switch (transp[i].type) {

					case "TRAM":
						$(btn).appendTo('#listeTram');
						$("#listeTram").hide();
						break;
					case "CHRONO":
						$(btn).appendTo("#listeChrono");
						$("#listeChrono").hide();
						break;
					case "PROXIMO":
						$(btn).appendTo("#listeProximo");
						$("#listeProximo").hide();
						break;
					case "FLEXO":
						$(btn).appendTo("#listeFlexo");
						$("#listeFlexo").hide();
						break;
					default:
						alert('type inconnu');

				}
			} else {
				if (id.includes(tougo)) { // on ajoute le transport à la liste TAG

					$(btn).appendTo("#listeTougo");
					$("#listeTougo").hide();

				}
				else {
					if (id.includes(tpv)) { // on ajoute le transport à la liste TAG

						$(btn).appendTo("#listeVoironnais");
						$("#listeVoironnais").hide();
					}
					else {
						if (id.includes(SNC)) { // on ajoute le transport à la liste TAG

							$(btn).appendTo("#listeSncf");
							$("#listeSncf").hide();
						}
						else {
							// autre cas : transière

							$(btn).appendTo("#listeTransisere");
							$("#listeTransisere").hide();
						}

					}
				}
			}
		}

	}

})
